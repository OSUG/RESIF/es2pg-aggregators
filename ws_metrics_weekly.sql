-- Création de la table ws_metrics_weekly
--
CREATE TABLE ws_metrics_weekly (
       date TIMESTAMP,
       service VARCHAR(32),
       response INTEGER,
       requests INTEGER,
       size BIGINT,
       clients HLL,
       CONSTRAINT date_service_response PRIMARY KEY (date, service, response)
)
