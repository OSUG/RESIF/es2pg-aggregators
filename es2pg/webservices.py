"""
Récupère tous les événements de la base elasticsearch pour en calculer des indicateurs et les stocker en base postgres
Les informations récupérées sont :

- @timestamp
- response
- bytes
- clientip
- request
- service

"""

import logging
import pprint
from datetime import date, datetime, timedelta

import click
import mmh3
import psycopg
from alive_progress import alive_bar
from opensearchpy import OpenSearch, Search
from python_hll.hll import HLL
from python_hll.util import NumberUtil

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("es2pg")
es = ""  ### Will be initialized on CLI call
es_logger = logging.getLogger("opensearch")
es_logger.setLevel(logging.WARNING)


class Metric:
    def __init__(
        self,
        service: str,
        d: date,
        resp: int = 200,
        req: int = 1,
        size: int = 0,
        clientip: str = "",
    ):
        self.service = service
        self.date = self._shift_date(d)
        self.response = resp
        self.requests = int(req)
        self.size = int(size)
        self.unique_clients = HLL(11, 5)
        if clientip != "":
            self.unique_clients.add_raw(mmh3.hash(clientip))

    def key(self):
        return f"{self.service}_{self.date}_{self.response}"

    def _shift_date(self, d: date) -> date:
        """
        Calcule la date du début de semaine
        """
        return d - timedelta(days=d.weekday())

    def __iadd__(self, metric):
        # check date
        if self.key() == metric.key():
            self.requests += metric.requests
            self.size += metric.size
            self.unique_clients.union(metric.unique_clients)
        else:
            logger.warning(
                "%s is not in my timerange (one week from %s on)",
                metric.date,
                self.date,
            )
            raise TypeError
        return self

    def serialize_to_pg(self):
        hlltobytes = self.unique_clients.to_bytes()
        clients_hash = "\\x" + NumberUtil.to_hex(hlltobytes, 0, len(hlltobytes))
        return (
            self.date,
            self.service,
            self.response,
            self.requests,
            self.size,
            clients_hash,
        )

    def __repr__(self):
        return f"service: {self.service}, date: {self.date}, response: {self.response}, requests: {self.requests}, size: {self.size}, clients: {self.unique_clients.cardinality()}"


class MetricsCollection:
    """
    Gestion de toutes les métriques
    """

    def __init__(self):
        self._statistics = {}

    def append(self, stat):
        # Si on a déjà une stat sur les mêmes clés, alors on ajoute
        if stat.key() in self._statistics:
            self._statistics[stat.key()] += stat
        else:
            self._statistics[stat.key()] = stat

    def to_list(self):
        return self._statistics.values()

    def to_pglist(self):
        return (k.serialize_to_pg() for k in self._statistics.values())

    def count(self):
        return len(self.to_list())

    def void(self):
        self._statistics = {}

    def __repr__(self):
        reprstr = ""
        for k in self._statistics.keys():
            reprstr += str(self._statistics[k]) + "\n"
        return reprstr


service_metrics = MetricsCollection()


def url_to_service(host, url, suffix="ws.resif.fr"):
    """
    Recompose une url et un nom d'hôte pour en faire un nom de service normalisé.
    - Si host est préfixé (avant ws.resif.fr), alors ce préfixe est ajouté au nom du service
    - sinon, pas de préfixe
    - on prend la deuxième partie de l'URL (on omet fdsnws, resifws, etc.)
    Par exemple :
    - (ws.resif.fr, /fdsnws/dataselect/1) => fdsnws-dataselect
    - (ph5ws.resif.fr, /resifws/availability/1) => ph5-resifws-availability
    """
    if host.endswith(suffix):
        prefix = host[: -len(suffix)]
        if prefix:
            prefix += "-"
    else:
        raise ValueError("host should end with 'ws.resif.fr'")
    try:
        endpoints = url.split("/")[1:3]
    except Exception:
        # service est malformmé ... on fait quoi ?
        raise ValueError(
            "url parameter is malformed. Expected form : '/prefix/service/1'"
        )
    return prefix + "-".join(endpoints)


def list_indices(prefix="*", opensearch=es):
    indices = opensearch.indices.get(prefix)
    logger.debug("Indices list: %s", indices)
    return sorted([k for k in indices.keys()])


def list_services(index, opensearch=es):
    """
    Se connecte à la base elasticsearch et renvoie la liste des valeurs du tag "service"
    :param index liste des index séparés par des ','
    """
    response = opensearch.search(
        index=index,
        body={
            "aggs": {"services": {"terms": {"field": "service.fullname.keyword"}}},
        },
    )
    return [v["key"] for v in response["aggregations"]["services"]["buckets"]]


def get_last_update(dburi):
    """
    Se connecte à la base de données de statistiques pour récupérer la date de dernière modification
    Renvoie un objet de type datetime
    """
    last_time = datetime.now() - timedelta(days=7)
    with psycopg.connect(dburi) as conn:
        with conn.cursor() as curs:
            curs.execute("SELECT max(date) FROM ws_metrics_weekly")
            if curs.rowcount == 1:
                last_time = curs.fetchone()[0]
    return last_time


def aggregate_es_data(index, services, start_date, end_date, limit):
    """
    Fait une recherche sur un service
    :param index : les index à consulter
    :param service : le service su lequel filtrer
    :param start_date : la date de début de la recherche. Soit un objet datetime.Date, soit une chaine de caractère au format YYYY-MM-DD
    :param end_date : la date de fin de recherche.
    """
    # Cette requête prend tous les documents qui ne sont pas des requêtes HEAD, comprises dans l'interval de temps spécifié
    result = (
        Search(using=es, index=index)
        .exclude("match", **{"http.request.method": "HEAD"})
        .filter("range", **{"@timestamp": {"gte": start_date, "lt": end_date}})
        .source(
            [
                "@timestamp",
                "http.request.method",
                "http.response.status_code",
                "http.response.bytes",
                "haproxy.http.request.captured_headers",
                "service.fullname",
                "source.address",
            ]
        )
    )
    if limit == 0:
        limit = result.count()
    logger.info("%s results to scan", limit)

    with alive_bar(limit) as pbar:
        for hit in result.scan():
            if limit == 0:
                break
            limit -= 1
            e = hit.to_dict()
            logger.debug("Retrieved document: %s", e)
            # On jette tout ce qui concerne Zabbix
            headers = (
                e.get("haproxy", {})
                .get("http", {})
                .get("request", {})
                .get("captured_headers", [])
            )
            if len(headers) >= 2:
                if headers[1] == "Zabbix":
                    logger.debug("Ignoring Zabbix requests")
                    continue
            service = e.get("service", {}).get("fullname", "")
            if service in services:
                metric = Metric(
                    service,
                    datetime.strptime(e["@timestamp"][0:10], "%Y-%m-%d"),
                    resp=e.get("http", {}).get("response", {}).get("status_code", 404),
                    size=e.get("http", {}).get("response", {}).get("bytes", 0),
                    clientip=e.get("source", {}).get("address", None),
                )
                service_metrics.append(metric)
            else:
                logger.debug("Service %s ignored", service)
            pbar()


@click.command()
@click.option(
    "--es-server",
    default="eli.univ-grenoble-alpes.fr",
    help="Opensearch server",
    envvar="ESHOST",
)
@click.option(
    "--es-urlprefix",
    default="opensearch-resif",
    help="Opensearch URL prefix",
    envvar="ESURL",
)
@click.option(
    "--es-port",
    default="443",
    help="Elasticsearch port",
    envvar="ESPORT",
    type=click.INT,
)
@click.option("--es-user", default="es2pg", help="Elasticsearch user", envvar="ESUSER")
@click.option("--es-pass", default="", help="Elasticsearch password", envvar="ESPASS")
@click.option(
    "--es-indexprefix",
    default="filebeat-*",
    help="Index prefix to scan in elasticsearch. Default: 'filebeat-*'",
    envvar="ESINDEX",
)
@click.option(
    "--dburi",
    default="postgresql://resifstats@resif-pgpreprod.u-ga.fr/resifstats",
    help="Postgres statistics database access",
    envvar="DBURI",
)
@click.option(
    "--start-date",
    default=None,
    help="From when to start the search (YYYY-MM-DD). If left empty we will guess the starting date from the database.",
)
@click.option(
    "--end-date",
    default=None,
    help="Until when to start the search (YYYY-MM-DD). Default is now.",
)
@click.option(
    "--services",
    "-s",
    "services_param",
    help="Coma separated list of services to gets stats from. If left empty, all services are scanned.",
    envvar="SERVICES",
    default="fdsnws-dataselect,fdsnws-station,fdsnws-availability,fdsnws-event,ph5-fdsnws-dataselect,eidaws-wfcatalog,resifws-timeseries,resifws-timeseriesplot,resifws-sacpz,resifws-resp,resifws-evalresp,resifws-seedpsd,resifws-statistics,ph5-resifws-availability,resifsi-transaction,resifsi-orphanfiles,resifsi-assembleddata",
)
@click.option("--verbose", "-v", help="verbose output", default=False, is_flag=True)
@click.option(
    "--noop",
    "-n",
    help="do not register the metrics in database",
    default=False,
    is_flag=True,
)
@click.option(
    "--limit",
    "-l",
    help="limit the number of elasticsearch events to fetch",
    type=int,
    default=0,
)
@click.option(
    "--sum-stats",
    help="If set, new stats will be added to existing statistics. Else, new stats overwrites existing.",
    is_flag=True,
    default=False,
)
def cli(
    es_server,
    es_urlprefix,
    es_port,
    es_user,
    es_pass,
    es_indexprefix,
    dburi,
    start_date,
    end_date,
    services_param,
    verbose,
    noop,
    limit,
    sum_stats,
):
    global es
    es = OpenSearch(
        hosts=[{"host": es_server, "port": es_port, "url_prefix": es_urlprefix}],
        use_ssl=True,
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=(es_user, es_pass),
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    if verbose:
        logger.setLevel(logging.DEBUG)
    logger.info("Starting aggregation")
    # 0. Lister les index de recherche
    # On limite le nombre d'indices
    indices = list_indices(es_indexprefix, opensearch=es)[-200:]
    logger.info("Indices to scan: %s", indices)
    if services_param is not None:
        services = sorted(services_param.split(","))
    # 1. Cherche la liste des services
    else:
        services = list_services(indices, opensearch=es)
    logger.info("Services to scan for: %s", services)
    # 2. Pour chaque service,
    #    - récupère dans postgres la date de début
    #    - fait une recherche elasticsearch entre le début et maintenant
    #    - constitue des statistiques mensuelles
    #    - met à jour dans postgres
    if start_date:
        last_time = date.fromisoformat(start_date)
    else:
        last_time = get_last_update(dburi)
    logger.info("We need to collect stats from %s on", last_time)
    if end_date is None:
        end_date = datetime.now()
    aggregate_es_data(indices, services, last_time, end_date, limit)
    if noop:
        logger.info("Not registering to database")
        pprint.PrettyPrinter(indent=4, compact=False)
        pprint.pp(service_metrics.to_pglist())
    else:
        logger.info("Inserting %s metrics in database", service_metrics.count())
        on_conflict_do = "UPDATE SET requests = EXCLUDED.requests, size = EXCLUDED.size, clients = EXCLUDED.clients"
        if sum_stats:
            on_conflict_do = """UPDATE SET requests = ws_metrics_weekly.requests + excluded.requests,
                                size = ws_metrics_weekly.size + excluded.size,
                                clients = ws_metrics_weekly.clients || excluded.clients
            """
        try:
            with psycopg.connect(dburi) as conn:
                logger.info(service_metrics.to_pglist())
                with conn.cursor() as curs:
                    curs.executemany(
                        f"""
                                    INSERT INTO ws_metrics_weekly (date, service, response, requests, size, clients)
                                    VALUES (%s,%s,%s,%s,%s,%s)
                                    ON CONFLICT ON CONSTRAINT date_service_response DO {on_conflict_do};
                                    """,
                        service_metrics.to_pglist(),
                    )
            logger.info("Done")
        except psycopg.Error as e:
            print("Error writing to postgres %s database" % (dburi))
            print(e)


if __name__ == "__main__":
    cli()
