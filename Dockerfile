FROM ghcr.io/astral-sh/uv:python3.13-bookworm-slim AS builder

# Enable bytecode compilation
# Copy from the cache instead of linking since it's a mounted volume
ENV UV_COMPILE_BYTECODE=1 \
    UV_LINK_MODE=copy

WORKDIR /app
# Sync only project dependencies
RUN --mount=type=cache,target=/root/.cache/uv \
    --mount=type=bind,source=uv.lock,target=uv.lock \
    --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
    uv sync --frozen --no-install-project --no-dev

FROM python:3.13-slim-bookworm
RUN addgroup --system app && \
    adduser --system --group app
USER app
WORKDIR /app
COPY --from=builder --chown=app:app /app /app
COPY es2pg .
ARG CI_COMMIT_SHORT_SHA
ENV PATH="/app/.venv/bin:$PATH"
CMD ["python", "/app/webservices.py"]
