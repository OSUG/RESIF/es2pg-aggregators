#!/usr/bin/env python3
"""
Get all events from elasticsearch indices and aggregates them in a file suitable to copy into postgres table.
"""


import logging
from datetime import date, timedelta
from functools import lru_cache
from io import StringIO
import re
import psycopg2
import mmh3
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from python_hll.hll import HLL
from python_hll.util import NumberUtil

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

class HebdoStat():
    def __init__(self, d=date(2020,1,1), net="", sta="", loc="", cha="", country="", clientip="", requests=0, size=0):
        # Convert date to the date of the previous monday
        self.date = d - timedelta(days=d.weekday())
        self.network = net
        self.station = sta
        self.location = loc
        self.channel = cha
        self.country = country
        self.unique_clients = HLL(11,5)
        self.requests = requests
        try: 
                self.size = int(size)
        except ValueError as err:
                logger.warning("%s not convertible to integer. Replaced by 0", size)
                self.size = 0
		
        self.unique_clients.add_raw(mmh3.hash(clientip))

    def key(self):
        return f"{self.date}_{self.network}_{self.station}_{self.location}_{self.channel}_{self.country}"

    def __iadd__(self, stat):
        # Adding two HebdoStats from same date
        if self.key() == stat.key():
            self.requests += stat.requests
            self.size += stat.size
            self.unique_clients.union(stat.unique_clients)
        else:
            raise TypeError
        return self

    def __repr__(self):
        return f"week: {self.date} ; net: {self.network} ; sta: {self.station} ; loc: {self.location}; chan: {self.channel} ; country: {self.country}; size: {self.size}, reqs: {self.requests} ; clients: {self.unique_clients.cardinality()}"

    def to_copy_string(self):
        if self.network == "":
            net = r"\N"
        else:
            net = self.network
        if self.station == "":
            sta = r"\N"
        else:
            sta = self.station
        if self.location == "":
            loc = r"\N"
        else:
            loc = self.location
        if self.channel == "":
            chan = r"\N"
        else:
            chan = self.channel
        if self.country == "":
            country = r"\N"
        else:
            country = self.country
        hlltobytes = self.unique_clients.to_bytes()
        return f"{self.date}\tstation\t{net}\t{sta}\t{loc}\t{chan}\t{country}\t\\\\x{NumberUtil.to_hex(hlltobytes, 0, len(hlltobytes))}\t{self.size}\t{self.requests}\n"


class HebdoStatsCollection():
    def __init__(self):
        self._statistics = {}
        pass

    def append(self, stat):
        # Si on a déjà une stat sur les mêmes clés, alors on ajoute
        if stat.key() in self._statistics:
            self._statistics[stat.key()] += stat
        else:
            self._statistics[stat.key()] = stat

    def to_list(self):
        return self._statistics.values()



@lru_cache(maxsize=10000)

@lru_cache(maxsize=10000)
def netcode_extender(net, sta=""):
    """
    Fonction qui essaye de deviner le code étendu d'un réseau avec sa station
    """
    extcode=net
    if sta == "":
        request = "select network,start_year,end_year from networks where network=%s"
        request_param = (net,)
    else:
        request = 'select networks.network,networks.start_year,networks.end_year from (select network_id from station where station=%s) as station inner join networks ON station.network_id = networks.network_id'
        request_param = (sta,)
    try:
        with psycopg2.connect('postgres://resifinv_ro@resif-pgprod.u-ga.fr:5432/resifInv-Prod') as conn:
            with conn.cursor() as curs:
                curs.execute(request, request_param)
                results = curs.fetchall()
                for r in results:
                    if r[0] == net:
                        if r[2] < 2500:
                            extcode = r[0]+str(r[1])
                            break
    except Exception as err:
        print(err)
    logger.info("%s, %s extended to %s",net, sta, extcode)
    return extcode


# Elasticsearch database access
es = Elasticsearch(['resif-elk.u-ga.fr'], port=9200)
indexpattern = "logstash-apache*"
count = es.count(index=indexpattern, body={"query": {"match_all": {}}})
print(f"{count} indices for {indexpattern}")
res = Search(using=es, index=indexpattern)\
    .filter("term", service="station")\
    .filter("term", response=200)\
    .filter("range",  **{'@timestamp': {'gte': '2019-01-01', 'lt': '2021-01-15'}})
# TODO : une première requête des entrées qui n'ont pas tag:{'_grokparsefailure}
# TODO : une seconde requête avec ceux qui ont _grokparsefailure pour rescanner tout.
# Créer une chaine de caractère avec tous les éléments séparés par \t
items = HebdoStatsCollection()

# On va bien identifier les paramètres qui ont été passés de manière unique
# Si la requête comporte des wildcards, ou plusieurs, on va pas analyser, juste comptabiliser la requête.
network_re = r"^[0-9A-Z]{1,2}$"
station_re = r"^[0-9A-Z]{3,5}$"
channel_re = r"^[A-Z][A-Z][1-3A-Z]$"

for hit in res.scan():
    e = hit.to_dict()
    if 'bytes' in e.keys():
        size = e['bytes']
    elif 'size' in e.keys():
        size = e['size']
    else:
        size = 0
    if 'station' in e and re.match(station_re, e['station']):
        sta = e['station']
    else:
        sta = ""
    if 'network' in e and re.match(network_re, e['network']):
        net = netcode_extender(e['network'],sta)
    else:
        net = ""
    if 'channel' in e and re.match(channel_re, e['channel']):
        chan = e['channel']
    else:
        chan = ""
    if 'geoip' not in e or 'country_code2' not in e['geoip']:
        e['geoip']={'country_code2':""}
    stat = HebdoStat(
        date.fromisoformat(e['@timestamp'][0:10]),
        net,
        sta,
        "",  # On laisse tomber ce paramètre location, pas envie de gérer les jokers
        chan,
        e['geoip']['country_code2'],
        e['clientip'],
        1,
        size
    )
    logger.debug("Creating new stat for %s at %s", e['@timestamp'][0:10], stat.date)
    items.append(stat)

strio = StringIO()
for i in items.to_list():
    strio.writelines(i.to_copy_string())
strio.seek(0)
# Postgres database
dburi='postgres://resifstats@resif-pgpreprod.u-ga.fr:5432/resifstats'
try:
    conn = psycopg2.connect(dburi)
    cur = conn.cursor()
    cur.copy_from(strio, 'sent_data_summary_weekly')
    cur.close()
    conn.commit()
    conn.close()
except Exception as e:
    print("Error writing to postgres %s database"%(dburi))
    print(e)
