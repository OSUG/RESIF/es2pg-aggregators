# es2pg_webservices.py

Permet de créer des agrégation hebdomadaires pour tous les webservices stockés dans elasticsearch.

Insert les aggrégats dans la table `ws_metrics_weekly`

## Utilisation

    python ./es2pg_webservices.py --help
    
Tous les paramètres peuvent être passés à l'aide de variable d'environnement. La règle de nommage est de les mettre en majuscule et de remplacer les `-` par des `_`. Par exemple `--es-server` -> `ES_SERVER`.

# es_station_export.py

Programme one-shot mais pas sans intérêt. Il doit rattraper des statistiques d'utilisation du ws station qui sont faites par `fdsnwsstat` depuis janvier 2021.

Ce programme récupère toutes les entrées dans elasticsearch concernant le webservice et précédant la date de mise en prod de `fsnwsstat`

Il fait au mieux pour renseigner des statistiques hebdomadaires, prépare un gros "fichier" qui sera injecter dans la base postgres.

## Utilisation

Construction du docker :

    docker build -t es2pg_station .

Lancement du docker. Il a besoin d'accéder à 2 bases de données, donc on prépare un fichier pgpass qu'on lui mettra à disposition :

    resif-pgprod.u-ga.fr:5432:resifstats:resifstats:XXXXXXXXX
    resif-pgprod.u-ga.fr:5432:resifInv-Prod:resifinv_ro:XXXXXXXXXX
    
Et on le met à dispo du container :

    docker run --rm -e PGPASSFILE=/pgpass -v $(pwd)/pgpass:/pgpass es2pg_station
