from es2pg import webservices
from opensearchpy import OpenSearch, Search


es = OpenSearch(
    hosts=[
        {
            "host": "eli.univ-grenoble-alpes.fr",
            "port": 443,
            "url_prefix": "opensearch-resif",
        }
    ],
    use_ssl=True,
    http_compress=True,  # enables gzip compression for request bodies
    http_auth=("es2pg", ""),
    ssl_assert_hostname=False,
    ssl_show_warn=False,
)


def test_list_indices():
    indices = webservices.list_indices("vector-*", opensearch=es)
    assert len(indices) > 0


def test_list_services(capsys):
    indices = webservices.list_indices("vector-*", opensearch=es)
    srvs = webservices.list_services(indices[-1:], opensearch=es)
    with capsys.disabled():
        print(f"List of services {srvs}")
    assert True
